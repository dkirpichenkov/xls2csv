/**
 * Created with IntelliJ IDEA.
 * User: doki
 * Date: 23.04.13
 * Time: 23:21
 * To change this template use File | Settings | File Templates.
 */
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ExcelReading {

    public static void echoAsCSV(Sheet sheet, int header, int footer) throws FileNotFoundException {
        Row row = null;
        File csv = new File("1.csv");
        PrintWriter out = new PrintWriter(csv);
        for (int i = sheet.getFirstRowNum()+header; i <= sheet.getLastRowNum()-(footer); i++) {
            row = sheet.getRow(i);

            if(row==null)
                continue;

            for (int j = 0; j < row.getLastCellNum(); j++) {
                out.print("\"" + row.getCell(j) + "\";");
            }
            out.println();
        }

        out.close();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        InputStream inp = null;
        try {
            inp = new FileInputStream("1.xlsx");
            Workbook wb = WorkbookFactory.create(inp);

            System.out.println(wb.getSheetAt(0).getSheetName());
            echoAsCSV(wb.getSheetAt(0), 0, 0);

        } catch (InvalidFormatException ex) {
            Logger.getLogger(ExcelReading.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ExcelReading.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ExcelReading.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                inp.close();
            } catch (IOException ex) {
                Logger.getLogger(ExcelReading.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}